%define name ostree_assembly
%define version 0.1
%define unmangled_version 0.1
%define unmangled_version 0.1
%define release 13

Summary: A web tool to build an ostree image.
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
Source1: ostree_assembly.service
Source2: ostass
Source3: db.sqlite3 

Patch1: 0001-fixed-download.patch
Patch2: 0001-fixed-message-passing-with-redis.patch
Patch3: 0001-fixed-celery-task-for-ostree-version-status.patch 
Patch4: 0001-remove-mqtt.patch
Patch5: 0001-fixed-init-config.patch
Patch6: 0001-fixed-requirments-and-suit-to-python3.patch
Patch7: 0001-reset-config-path.patch
Patch8: 0001-update-config.ini-edit-page.patch
Patch9: 0001-update-ostree-package-manage-page.patch
Patch10: 0001-update-main-json-configuration-page.patch
Patch11: 0001-support-to-edit-compose-post-cfg.patch
Patch12: 0001-support-download-cfgs.patch
Patch13: 0001-support-upload-cfgs.patch

License: GPLv3
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: JokerMa <mazhiguo@kylinos.com.cn>
Url: www.kylinsec.com.cn

BuildRequires: python3
BuildRequires: python3-setuptools 
Requires: python3 
Requires: rpm-ostree 
Requires: ostree 
Requires: rpm-ostree-toolbox 
Requires: redis
Requires: rabbitmq-server

%description
A web tool for building a ostree installer image.

%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{unmangled_version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1

%build
python3 setup.py build

%install
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system/ $RPM_BUILD_ROOT/usr/bin
python3 setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
install -Dm755 %{SOURCE1} $RPM_BUILD_ROOT/usr/lib/systemd/system/ostree_assembly.service
install -Dm755 %{SOURCE2} $RPM_BUILD_ROOT/usr/bin/ostass
install -Dm755 %{SOURCE3} $RPM_BUILD_ROOT%{python3_sitelib}/ostree_assembly/

%clean
rm -rf $RPM_BUILD_ROOT

%post
mkdir -p /opt/ostree_cfgs/ /home/ostree/OstreeRepo /var/log/ostree_logs/ /home/ostree/iso/ /home/ostree/StaticDelta/


%postun
rm -rf /opt/ostree_cfgs/ /home/ostree/OstreeRepo /var/log/ostree_logs/ /home/ostree/iso/ /home/ostree/StaticDelta/

%files -f INSTALLED_FILES
%defattr(-,root,root)
/usr/lib/systemd/system/ostree_assembly.service
/usr/bin/ostass

%changelog
* Wed May 24 2023 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-13
- KYOS-F: support upload cfgs

* Wed May 10 2023 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-12
- KYOS-F: support to download cfgs

* Sat Apr 22 2023 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-11
- KYOS-F: support to edit compose post cfg

* Sun Dec 17 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-10
- KYOS-F: update main json configuration page

* Mon Sep 19 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-9
- KYOS-F: update ostree package manage page

* Thu Sep 15 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-8
- KYOS-F: update config.ini edit page

* Wed Jul 06 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-7
- KYOS-F: reset config path

* Wed Jul 06 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-6
- KYOS-F: fixed requirments and suit to python3

* Wed Jun 22 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-5
- KYOS-F: fixed init config

* Mon Jun 06 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-4
- KYOS-F: remove mqtt

* Sat May 21 2022 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-3
- KYOS-F: fixed celery task for ostree version status

* Mon May 31 2021 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-2
- KYOS-F: 0001-fixed-message-passing-with-redis.patch

* Thu Nov 12 2020 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-2
- KYOS-F: 0001-fixed-download.patch

* Wed Nov 11 2020 mazhiguo <mazhiguo@kylinos.com.cn> -0.1-1
- KYOS-F: init code

