# ostree_assembly

#### 介绍
ostree_assembly 是一款ostree Web生成工具．

#### 更新说明
由于方案需要进行较大升级改动，本sig组拟在2022年8月左右方案确定后再做后续更新。

#### 安装教程

1.  下载srpm包编译，安装编译后的二进制包
2.  pip３ -r requirements.txt　安装所需要的python依赖

#### 使用说明

1.  启动redis: systemctl start redis
2.  启动ostree_assembly: systemctl start ostree_assembly
3.  浏览器访问　127.0.0.1:9003 (其它机器访问需防火墙开放9003端口)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



Copyright  2020 KylinSec. All rights reserved. 
版权声明： 2020 KylinSec.保留所有权利。
